﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VictoryAnimHelper : MonoBehaviour
{
    [SerializeField]
    Image[] titleImages;

    int selectedTitle;
    public void SelectRandomTitle()
    {
        selectedTitle = Random.Range(0, titleImages.Length);
        titleImages[selectedTitle].enabled = true;
    }

    public void DeselectTitle()
    {
        titleImages[selectedTitle].enabled = false;
    }
}
