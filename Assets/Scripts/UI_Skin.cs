﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Skin : MonoBehaviour
{
    [SerializeField]
    private GameObject buyButton;

    private Image skinBorderImage;

    [SerializeField]
    private int skinCost = 100;

    [SerializeField]
    private int skinNr;

    private void Start()
    {
        skinBorderImage = GetComponent<Image>();

        skinNr = transform.GetSiblingIndex();


        if (GlobalSetting.Instance.isSkinUnlocked(skinNr))
        {
            skinBorderImage.color = (GlobalSetting.Instance.selectedSkin == skinNr) ? Color.white : GlobalSetting.Instance.deselectedSkinColor;
            skinBorderImage.sprite = (GlobalSetting.Instance.selectedSkin == skinNr) ? GlobalSetting.Instance.roundedBorderChecked : GlobalSetting.Instance.roundedBorder;
            GetComponent<Button>().interactable = true;
            buyButton.SetActive(false);
        }
        else
        {
            GetComponent<Button>().interactable = false;
            buyButton.SetActive(true);
            buyButton.GetComponent<Button>().interactable = skinCost <= GlobalSetting.Instance.coins;
            buyButton.transform.GetChild(0).GetComponent<Text>().text = skinCost.ToString();
        }
    }

    public void BuySkin()
    {
        GlobalSetting.Instance.AddCoins(-skinCost);
        PlayerPrefs.SetInt("skin_" + skinNr, 1);
        buyButton.SetActive(false);

        foreach (var skinNode in FindObjectsOfType<UI_Skin>())
        {
            skinNode.CheckVisuals();
        }

    }

    public void SelectSkin()
    {
        GlobalSetting.Instance.SetTexture(skinNr);
        GlobalSetting.Instance.selectedSkin = skinNr;

        foreach (var skinNode in FindObjectsOfType<UI_Skin>())
        {
            skinNode.CheckVisuals();
        }
    }

    public void CheckVisuals()
    {
        Start();
    }
}
