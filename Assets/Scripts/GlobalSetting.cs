﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalSetting : MonoBehaviour
{
    public static GlobalSetting Instance;

    #region References

    public Material selectedMaterial;
    public Material deselectedMaterial;

    public float cupYModifier = 0.3f;
    public float cupYStep = 0.05f;

    [Header("Animation settings")]
    public int animationFps = 60;
    [Tooltip("Cup bounce animation length in seconds.")]
    public float animationLength = 2.0f;

    public AnimationCurve cup_position_Y_Curve;

    public AnimationCurve cup_position_XZ_rot_Curve;

    public AnimationCurve cup_scale_Y_Curve;

    public AnimationCurve cup_scale_XZ_Curve;

    public AnimationCurve block_position_Y_Curve;

    [Tooltip("Percentage when the cup animation touches the ground.")]
    [Range(0, 1.0f)]
    public float groundPercentage = 0.8f;

    public ParticleSystem dustParticles;

    public CupController[] victoryPositions;

    [Header("Skin settings"), SerializeField]
    private Material skinMaterial;

    [SerializeField]
    private Texture[] skinTextures;

    public Color deselectedSkinColor;

    public Sprite roundedBorder;
    public Sprite roundedBorderChecked;

    [Header("UI settings")]
    public ParticleSystem victoryParticles;
    [SerializeField]
    private Animator victoryAnimator;

    public Animation coinAnimation;

    [SerializeField]
    private Text coinText;

    [SerializeField]
    private Color compleatedNodeColor;
    [SerializeField]
    private Image[] sliderLevelNodes;

    [Header("Game settings")]
    [SerializeField]
    private GameObject[] levelPresets;
    [SerializeField]
    private GameObject[] tutorials;


    [HideInInspector]
    public bool gameStarted = false;
    [HideInInspector]
    public int cupsLeftOnLevel;

    #endregion

    #region PlayerStatistics
    public int coins
    {
        get
        {
            return PlayerPrefs.GetInt("coins", 0);
        }
        set
        {
            PlayerPrefs.SetInt("coins", value);
        }
    }

    public int level
    {
        get
        {
            return PlayerPrefs.GetInt("level", 0);
        }
        set
        {
            PlayerPrefs.SetInt("level", value);
        }
    }

    public int selectedSkin
    {
        get
        {
            return PlayerPrefs.GetInt("skin", 0);
        }
        set
        {
            PlayerPrefs.SetInt("skin", value);
        }
    }

    public bool isSkinUnlocked(int skinNr)
    {
        return PlayerPrefs.GetInt("skin_" + skinNr, 0) == 1;
    }

    #endregion

    void Awake()
    {
        Instance = this;
        PlayerPrefs.SetInt("skin_0", 1);

        foreach (var pos in victoryPositions)
        {
            pos.Init();
        }

        for (int i = 0; i < level; i++)
        {
            sliderLevelNodes[i].color = compleatedNodeColor;
        }

        InitUI();
        InitGame();
    }

    private void InitGame()
    {
        for (int i = 0; i < levelPresets.Length; i++)
        {
            levelPresets[i].SetActive(i == level);
        }
    }

    public void ShowVictoryScreen()
    {
        levelPresets[level].GetComponent<Animator>().SetTrigger("slide_out");

        victoryParticles.Play();
        victoryAnimator.SetBool("start", true);
        Vibration.Vibrate(200);
    }

    public void GoToNextLevel()
    {
        cupsLeftOnLevel = 0;
        victoryParticles.Stop();
        victoryAnimator.SetBool("start", false);

        levelPresets[level].SetActive(false);
        sliderLevelNodes[level].color = compleatedNodeColor;

        level++;
        if (level >= levelPresets.Length)
        {
            level = 0;
            Application.LoadLevel(Application.loadedLevel);
        }

        levelPresets[level].SetActive(true);

        gameStarted = false;
    }

    private void InitUI()
    {
        coinText.text = coins.ToString();
    }

    public void ToggleTutorial(bool value)
    {
        if (level < tutorials.Length)
        {
            tutorials[level].gameObject.SetActive(value);
        }
    }

    public void SetTexture(int nr)
    {
        skinMaterial.mainTexture = skinTextures[nr];
    }

    public void AddCoins(int value)
    {
        coinAnimation.Play();
        coins += value;
        coinText.text = coins.ToString();
    }
}
