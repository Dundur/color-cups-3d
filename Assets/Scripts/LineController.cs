﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LineController : MonoBehaviour
{
    private Vector3 mOffset;
    private float mZCoord;

    private LineRenderer lr;
    private List<SelectableBlock> selectedBlocks;

    float idleTime = 2.0f;
    float idleTimer = 0;

    float distanceValue = 1.414214f;

    void Start()
    {
        lr = GetComponent<LineRenderer>();
        selectedBlocks = new List<SelectableBlock>();
        idleTimer = 0;
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            GlobalSetting.Instance.level = 0;
            Application.LoadLevel(Application.loadedLevel);
        }

        if (GlobalSetting.Instance.gameStarted)
            return;

        if (Input.GetMouseButton(0))
        {
            MouseDrag();
        }
        if (Input.GetMouseButtonUp(0))
        {
            MouseUp();
        }

        if (idleTimer > idleTime)
        {
            GlobalSetting.Instance.ToggleTutorial(true);
        }
        else
        {
            idleTimer += Time.deltaTime;
        }
    }

    void MouseDrag()
    {
        idleTimer = 0;
        GlobalSetting.Instance.ToggleTutorial(false);


        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, 100))
        {
            SelectableBlock block = hit.transform.GetComponent<SelectableBlock>();

            if (block == null)
                return;

            if (selectedBlocks.Count < 1 )
            {
                AddBlockAndUpdateLineRenderer(block);
            }
            else
            {
                if (!selectedBlocks.Contains(block)
                && 
                // Занести в метод
                Vector3.Distance(selectedBlocks[selectedBlocks.Count - 1].transform.position, hit.transform.position) <= distanceValue)
                {
                    AddBlockAndUpdateLineRenderer(block);
                }
                else if (selectedBlocks.Contains(block) && selectedBlocks[selectedBlocks.Count - 1] != block)
                {
                    var startIndex = selectedBlocks.IndexOf(block);
                    var removeCount = selectedBlocks.Count - startIndex;

                    for (int i = startIndex; i < selectedBlocks.Count; i++)
                    {
                        selectedBlocks[i].Deselect();
                    }

                    selectedBlocks.RemoveRange(startIndex, removeCount);
                    lr.positionCount = selectedBlocks.Count;
                }
            }
        }
    }

    private void AddBlockAndUpdateLineRenderer(SelectableBlock _block)
    {
        selectedBlocks.Add(_block);
        lr.positionCount = selectedBlocks.Count;
        _block.Select();
        lr.SetPosition(selectedBlocks.Count - 1, new Vector3(_block.transform.position.x, -0.2f, _block.transform.position.z));
    }

    void MouseUp()
    {
        idleTimer = 0;
        GlobalSetting.Instance.ToggleTutorial(false);


        if (selectedBlocks.Count == FindObjectsOfType<SelectableBlock>().Length)
        {
            for (int i = 0; i < selectedBlocks.Count - 1; i++)
            {
                selectedBlocks[i].attachedCup.SetTarget(selectedBlocks[i + 1].attachedCup);
            }

            selectedBlocks[selectedBlocks.Count - 1].attachedCup.Init();

            selectedBlocks[0].attachedCup.StartBounceAnimation(0);
            GlobalSetting.Instance.gameStarted = true;
        }

        foreach (var block in selectedBlocks)
        {
            block.Deselect();
        }

        selectedBlocks.Clear();
        lr.positionCount = 0;
    }
}
