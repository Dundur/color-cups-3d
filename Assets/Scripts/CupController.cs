﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CupController : MonoBehaviour
{
    #region Aniamtion

    private float posYVal { get { return GlobalSetting.Instance.cup_position_Y_Curve.Evaluate(animationTimeNormalized); } }
    private float posXZRotVal { get { return GlobalSetting.Instance.cup_position_XZ_rot_Curve.Evaluate(animationTimeNormalized); } }
    private float scaleYVal { get { return GlobalSetting.Instance.cup_scale_Y_Curve.Evaluate(animationTimeNormalized); } }
    private float scaleXZVal { get { return GlobalSetting.Instance.cup_scale_XZ_Curve.Evaluate(animationTimeNormalized); } }

    private float blockScaleVal { get { return GlobalSetting.Instance.block_position_Y_Curve.Evaluate((animationTimeNormalized - GlobalSetting.Instance.groundPercentage) / (1 - GlobalSetting.Instance.groundPercentage)); } }

    private Vector3 startPosition;
    private Vector3 startRotation;

    private CupController targetCup;
    private int cupIndex;

    private bool isAnimatingBounce = false;
    private bool isAnimatingVictory = false;
    private bool canBlowUpOnClick = false;

    private float animationLength;

    private float timePastFromLastAnimationFrame = 0;
    private float timePerAnimationFrame;

    /// <summary>
    /// Animation current time in seconds.
    /// </summary>
    private float animationTime = 0;

    /// <summary>
    /// Current animation time normalized, use this for interpolation's and curve evaluations.
    /// </summary>
    private float animationTimeNormalized { get { return animationTime / animationLength; } }

    private bool calledOnce = false;

    #endregion

    private SelectableBlock attachedBlock;
    private CupController childCup;

    public void Init()
    {
        startRotation = transform.eulerAngles;
        startPosition = transform.position;
    }

    private void Start()
    {
        GlobalSetting.Instance.cupsLeftOnLevel++;
        attachedBlock = transform.Find("Block").GetComponent<SelectableBlock>();
    }

    // Start animation
    public void SetTarget(CupController _targetCup)
    {
        startPosition = transform.position;
        startRotation = transform.eulerAngles;

        animationLength = GlobalSetting.Instance.animationLength;
        timePerAnimationFrame = 1.0f / GlobalSetting.Instance.animationFps;
        animationTime = 0;

        targetCup = _targetCup;
    }

    public void StartBounceAnimation(int index)
    {
        cupIndex = index;
        isAnimatingBounce = true;
        attachedBlock.transform.SetParent(transform.parent);
    }

    public void StartVictoryAnimation()
    {
        isAnimatingVictory = true;
    }

    void Update()
    {
        if (isAnimatingBounce)
        {
            if (timePastFromLastAnimationFrame < timePerAnimationFrame)
            {
                timePastFromLastAnimationFrame += Time.deltaTime;
            }
            else
            {
                AnimateBounce();
                timePastFromLastAnimationFrame = 0;
            }
        }

        if (isAnimatingVictory)
        {
            if (timePastFromLastAnimationFrame < timePerAnimationFrame)
            {
                timePastFromLastAnimationFrame += Time.deltaTime;
            }
            else
            {
                AnimateVictory();
                timePastFromLastAnimationFrame = 0;
            }
        }
    }

    IEnumerator SlowMotion()
    {
        float i = 0;

        while (i < 1)
        {
            i += timePerAnimationFrame * 2;
            Time.timeScale = Mathf.Lerp(0.5f, 1f, i);
            yield return new WaitForSeconds(timePerAnimationFrame);
        }

        if (targetCup.targetCup != null)
        {
            targetCup.StartBounceAnimation(0);
        }
        else
        {
            // Last cup animation => Transition to victory animation
            targetCup.cupIndex = 0;
            targetCup.attachedBlock.transform.SetParent(targetCup.transform.parent);
            targetCup.SetToVictoryPosition();
        }
    }

    private void AnimateBounce()
    {
        animationTime += timePastFromLastAnimationFrame;

        if (animationTime >= animationLength)
        {
            // Last animaion frame (Animation end's)
            animationTime = animationLength;
            isAnimatingBounce = false;
        }


        if (animationTimeNormalized >= GlobalSetting.Instance.groundPercentage)
        {
            if (!calledOnce)
            {
                GlobalSetting.Instance.dustParticles.transform.position = targetCup.transform.position;
                GlobalSetting.Instance.dustParticles.Play();
                Vibration.Vibrate(40);
                calledOnce = true;
            }

            targetCup.attachedBlock.transform.position = new Vector3(targetCup.attachedBlock.transform.position.x, GlobalSetting.Instance.cupYModifier, targetCup.attachedBlock.transform.position.z) + Vector3.up * blockScaleVal;

            if (startRotation.y == targetCup.startRotation.y)
            {
                BlowUp();
                isAnimatingBounce = false;
                StartCoroutine(SlowMotion());
                return;
            }
        }

        transform.position = GetCurrentPosition();
        transform.eulerAngles = Vector3.Lerp(startRotation, new Vector3(0, startRotation.y == 0 ? 180 : 0, startRotation.z == 0 ? 180 : 0), posXZRotVal);
        transform.localScale = new Vector3(scaleXZVal, scaleYVal, scaleXZVal);

        if (!isAnimatingBounce)
        {
            if (targetCup.targetCup != null)
            {
                // Next cup animation
                targetCup.childCup = this;
                transform.SetParent(targetCup.transform);
                targetCup.StartBounceAnimation(cupIndex + 1);
            }
            else
            {
                // Last cup animation => Transition to victory animation
                targetCup.cupIndex = cupIndex + 1;
                targetCup.attachedBlock.transform.SetParent(targetCup.transform.parent);
                targetCup.childCup = this;
                targetCup.SetToVictoryPosition();
            }
        }
    }

    private void AnimateVictory()
    {
        animationTime += timePastFromLastAnimationFrame;

        if (animationTime >= animationLength)
        {
            // Last animaion frame (Animation end's)
            animationTime = animationLength;
            isAnimatingVictory = false;
            transform.position = targetCup.startPosition;
            transform.eulerAngles = targetCup.startRotation;

            canBlowUpOnClick = true;
            return;
        }

        transform.position = GetCurrentPosition();
        transform.eulerAngles = Vector3.Lerp(startRotation, targetCup.startRotation, posXZRotVal);
        transform.localScale = new Vector3(scaleXZVal, scaleYVal, scaleXZVal);
    }

    private Vector3 GetCurrentPosition()
    {
        return new Vector3(
            // X
            Mathf.Lerp(startPosition.x, targetCup.startPosition.x, posXZRotVal),
            // Y
            (targetCup.startPosition.y + (cupIndex + 1) * GlobalSetting.Instance.cupYStep) * posYVal,
            // Z
            Mathf.Lerp(startPosition.z, targetCup.startPosition.z, posXZRotVal)
            );
    }

    private void BlowUp(float multiplier = 1)
    {
        Vibration.Vibrate(20);

        GlobalSetting.Instance.cupsLeftOnLevel--;
        if (GlobalSetting.Instance.cupsLeftOnLevel < 1)
        {
            GlobalSetting.Instance.ShowVictoryScreen();
        }

        var rb = gameObject.AddComponent<Rigidbody>();
            rb.AddForce(Random.Range(-150, 150), 250 * multiplier, Random.Range(-150, 150));
        rb.AddTorque(Random.rotation.eulerAngles);

        if (childCup != null)
        {
            childCup.BlowUp();
        }

        Destroy(gameObject, 2.5f);
    }

    private void SetToVictoryPosition()
    {
        transform.SetParent(GlobalSetting.Instance.transform.parent);
        SetTarget(GlobalSetting.Instance.victoryPositions[cupIndex]);
        StartVictoryAnimation();

        if (childCup != null)
        {
            childCup.SetToVictoryPosition();
        }
    }

    private void OnMouseOver()
    {
        if (canBlowUpOnClick)
        {
            childCup = null;
            BlowUp(2);
            canBlowUpOnClick = false;

            GlobalSetting.Instance.AddCoins(2);
        }
    }
}
