﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectableBlock : MonoBehaviour
{
    [HideInInspector]
    public CupController attachedCup;
    private MeshRenderer mr;

    private void Start()
    {
        mr = GetComponent<MeshRenderer>();
        attachedCup = transform.parent.GetComponent<CupController>();
    }

    public void Select()
    {
        mr.material = GlobalSetting.Instance.selectedMaterial;
        Vibration.Vibrate(20);
    }

    public void Deselect()
    {
        mr.material = GlobalSetting.Instance.deselectedMaterial;
    }

    bool playOnce = false;
}


